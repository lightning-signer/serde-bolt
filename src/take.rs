use bitcoin::io::{self, Read};
use alloc::boxed::Box;
use core::cmp::min;

/// A reader that reads at most `limit` bytes from the underlying reader.
///
/// Unlike `std::io::Take`, this doesn't require `Sized`.
pub struct Take<'a, R: Read + ?Sized> {
    inner: Box<&'a mut R>,
    limit: u64,
}

impl<'a, R: Read + ?Sized> Take<'a, R> {
    /// Create a new `Take` reader that reads at most `limit` bytes from `reader`
    pub fn new(reader: Box<&'a mut R>, limit: u64) -> Self {
        Take {
            inner: reader,
            limit,
        }
    }

    /// Returns true if the underlying reader has been exhausted
    pub fn is_empty(&self) -> bool {
        self.limit == 0
    }

    /// Returns the number of bytes remaining to be read
    pub fn remaining(&self) -> u64 {
        self.limit
    }
}

impl<'a, R: Read + ?Sized> Read for Take<'a, R> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, io::Error> {
        if self.limit == 0 {
            return Ok(0);
        }

        let max = min(self.limit, buf.len() as u64) as usize;
        let n = self.inner.read(&mut buf[..max])?;

        self.limit -= n as u64;

        Ok(n)
    }
}
